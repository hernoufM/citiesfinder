#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "City.h"

City* creerCity(char* name, double latitude, double longitude){
	City *city = (City*) malloc(sizeof(City));
	if(!city)
		return NULL;
	city->name = (char*) malloc(strlen(name)+1);
	if(!city->name)
	{
		free(city);
		return NULL;
	}
	memcpy(city->name, name, strlen(name)+1);
	city->latitude = latitude;
	city->longitude = longitude;
	return city;
}

void libererCity(City* city){
	free(city->name);
	free(city);
}

char* nameCity(const City* city){
	return city->name;
}

double latitudeCity(const City* city){
	return city->latitude;
}

double longitudeCity(const City* city){
	return city->longitude;
}