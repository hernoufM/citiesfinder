#include <stdio.h>
#include <stdlib.h>
#include "intersect.h"

LinkedList* intersect(const LinkedList* listA, const LinkedList* listB,
                      int comparison_fn_t(const void *, const void *))	
{
	LinkedList* intersection = newLinkedList();
	if(!intersection)
	{
		fprintf(stderr, "Erreur d'allocation memoire pour la liste d'intersection\n");
		return NULL;
	}

	LLNode* nodeA = headOfLinkedList(listA);
	while(nodeA != NULL)
	{
		LLNode* nodeB = headOfLinkedList(listB);
		while(nodeB != NULL && comparison_fn_t(valueLLNode(nodeA), valueLLNode(nodeB))!=0)
		{
			nodeB = nextLLNode(nodeB);
		}
		if(nodeB!=NULL)
		{
			bool error = !insertInLinkedList(intersection, valueLLNode(nodeB));
			if(error)
			{
				fprintf(stderr,"Erreur d'allocation memoire dans linked list d'intersection\n");
				freeLinkedList(intersection, false);
				return NULL;
			}
		}
		nodeA = nextLLNode(nodeA);
	}

	return intersection;
}