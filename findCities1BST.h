#ifndef _FIND_CITIES_1BST_H
#define _FIND_CITIES_1BST_H

#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "City.h"

LinkedList* findCities(LinkedList*, double, double, double, double);

#endif