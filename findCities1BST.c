#include <stdio.h>
#include <stdlib.h>
#include "findCities1BST.h"

double latMin;
double latMax;
double lonMin;
double lonMax;

static int comparaisonDouble(const void* a, const void* b){
	double *ptA = (double*) a, *ptB = (double*) b;
	if(*ptA>*ptB)
	{
		return 1;
	}
	else if(*ptB>*ptA)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static bool keepIt(const void* ville){
	City* city = (City*) ville;
	if(longitudeCity(city) >= lonMin && longitudeCity(city) <= lonMax){
		return true;
	}
	return false;
}

LinkedList* findCities(LinkedList* cities, double latitudeMin, double latitudeMax, double longitudeMin, double longitudeMax){
	latMin = latitudeMin;
	latMax = latitudeMax;
	lonMin = longitudeMin;
	lonMax = longitudeMax;

	//creation d'arbre
	printf("\nCreation d'un arbre avec latitude comme cle...\n");
	BinarySearchTree *citiesBTS = newBST(comparaisonDouble);
	if (!citiesBTS)
	{
		fprintf(stderr, "Erreur d'allocation memoire pour arbre\n");
		return NULL;
	}
	printf("Arbre est cree.\nReplissage par villes...\n");
	
	//remplissage d'arbre
	LLNode* llnode = headOfLinkedList(cities);
	int cpt=1;
	while(llnode != NULL)
	{
		City* city = (City*) valueLLNode(llnode);
		bool error = !insertInBST(citiesBTS, &(city->latitude), city);
		if(error)
		{
			fprintf(stderr, "Erreur d'allocation memoire dans arbre pour la ville numero %d\n", cpt);
			freeBST(citiesBTS, false);
			return NULL;
		}
		llnode = nextLLNode(llnode);
		cpt++;
	}
	printf("Arbre est rempli.\nFiltration selon latitude dans linked list...\n");
	
	//filtration selon la cle (latitude)
	LinkedList *filtredLatitude = getInRange(citiesBTS, &latitudeMin, &latitudeMax);
	if(!filtredLatitude)
	{
		fprintf(stderr, "Erreur de filtration selon latitude\n");
		freeBST(citiesBTS, false);
		return NULL;
	}
	printf("Les villes sont filtrees dans linked list.\nLiberation de memoire alloue par arbre...\n");
	freeBST(citiesBTS, false);
	printf("L'espace memoire est libere.\nFiltration selon longitude dans nouveau linked list...\n");

	//filtration selon longitude
	LinkedList* filtred = filterLinkedList(filtredLatitude, keepIt);

	printf("Les villes sont filtrees selon longitude.\nLiberation de memoire alloue par ancien linked list...\n");
	freeLinkedList(filtredLatitude, false);
	printf("L'espace memoire est libere.\n");
	return filtred;
}