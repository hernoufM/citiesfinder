/* ========================================================================= *
 * City interface
 * ========================================================================= */

#ifndef _CITY_H_
#define _CITY_H_

/** Data structure for storing city information */
typedef struct city_t {
    char* name;
    double latitude; // latitude
    double longitude; // longitude
} City;

City* creerCity(char*, double, double);
void libererCity(City*);
char* nameCity(const City*);
double latitudeCity(const City*);
double longitudeCity(const City*);
#endif // !_CITY_H_
