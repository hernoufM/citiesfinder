#ifndef _NODE_BST_H_
#define _NODE_BST_H_

#include <stdbool.h>

typedef struct nodeBST_t {
	const void* key;
	const void* value;
	struct nodeBST_t* fg;
	struct nodeBST_t* fd;
} NodeBST;

NodeBST* creerNodeBST(const void*, const void*);
void libererNodeBST(void*, bool);
const void*  keyNodeBST(const NodeBST*);
const void*  valueNodeBST(const NodeBST*);
NodeBST* FGNodeBST(const NodeBST*);
NodeBST* FDNodeBST(const NodeBST*);
void affecteKeyNodeBST(NodeBST*, const void*);
void affecteValueNodeBST(NodeBST*, const void*);
void affecteFGNodeBST(NodeBST*, NodeBST*);
void affecteFDNodeBST(NodeBST*, NodeBST*);

#endif