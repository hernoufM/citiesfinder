#ifndef _KD_BINARY_SEARCH_TREE_H_
#define _KD_BINARY_SEARCH_TREE_H_

#include <stddef.h>
#include <stdbool.h>
#include "kdNodeBST.h"
#include "LinkedList.h"

typedef struct kdTree_t {
	int nombreDimensions;
	kdNodeBST* tree;
	int (**tabFunctionComparison)(const void*, const void*);
} kdBinarySearchTree;

kdBinarySearchTree* newkdBST(int, int (**comparison_fn_t)(const void *, const void *));
void freekdBST(kdBinarySearchTree*, bool);
int nombreDimensionskdBST(const kdBinarySearchTree*);
kdNodeBST* treekdBST(const kdBinarySearchTree*);
void affecteNombreDimensionkdBST(kdBinarySearchTree*, int);
void affecteTreekdBST(kdBinarySearchTree*, kdNodeBST*);
size_t sizeOfkdBST(const kdBinarySearchTree*);
bool insertInkdBST(kdBinarySearchTree*, const void**, const void*);
//const void* searchkdBST(kdBinarySearchTree*, const void*);
LinkedList* getkdInRange(const kdBinarySearchTree*, void**, void**);

 #endif