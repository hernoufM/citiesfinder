/* ========================================================================= *
 * findCities: list implementation
 * ========================================================================= */

#include <stdio.h>
#include <stdlib.h>
#include "findCitiesList.h"

LinkedList* findCities(LinkedList* cities,
                       double latitudeMin,
                       double latitudeMax,
                       double longitudeMin,
                       double longitudeMax)
{
    // Create return object
    printf("Creation d'un linked list...\n");
    LinkedList* filtered = newLinkedList();
    if (!filtered)
        return NULL;
    printf("Linked list est cree.\nFiltration des villes dans cette linked list...\n");
    // Filter the cities
    LLNode* curr = headOfLinkedList(cities);
    bool error = false;
    const City* city;
    while(!error && curr != NULL)
    {
        city = (const City*) valueLLNode(curr);
        if(latitudeMin <= latitudeCity(city) && latitudeCity(city) <= latitudeMax &&
            longitudeMin <= longitudeCity(city) && longitudeCity(city) <= longitudeMax)
        {
            error = !insertInLinkedList(filtered, valueLLNode(curr));
        }
        curr = nextLLNode(curr);
    }
    // Free in case of error
    if(error)
    {
        fprintf(stderr, "Erreur d'insertion de la ville dans linked list\n");
        freeLinkedList(filtered, false);
        return NULL;
    }
    return filtered;
}