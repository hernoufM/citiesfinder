#include <stdio.h>
#include <stdlib.h>
#include "BinarySearchTree.h"

BinarySearchTree* newBST(int comparison_fn_t(const void *, const void *)) {
	BinarySearchTree* bst = (BinarySearchTree*) malloc(sizeof(BinarySearchTree));
	if(!bst)
		return NULL;
	affecteTreeBST(bst, NULL);
	bst->functionComparison = comparison_fn_t;
	return bst;
}

void freeBST(BinarySearchTree* bst, bool freeContent) {
	if(bst)
	{
		NodeBST* node = treeBST(bst);
		if(node)
		{
			BinarySearchTree* bstFG = newBST(bst->functionComparison), *bstFD = newBST(bst->functionComparison);
			affecteTreeBST(bstFG, FGNodeBST(node));
			affecteTreeBST(bstFD, FDNodeBST(node));
			freeBST(bstFG, freeContent);
			freeBST(bstFD, freeContent);
			libererNodeBST(treeBST(bst), freeContent);
		}
		free(bst);
	}
}

NodeBST* treeBST(const BinarySearchTree* bst) {
	return bst->tree;
}

void affecteTreeBST(BinarySearchTree* bst, NodeBST* node){
	bst->tree = node;
}

size_t sizeOfBST(const BinarySearchTree* bst){
	NodeBST* node = treeBST(bst);
	if(!node)
	{
		return 0;
	}
	else
	{
		BinarySearchTree *bstFG = newBST(bst->functionComparison), *bstFD = newBST(bst->functionComparison);
		affecteTreeBST(bstFG, FGNodeBST(node));
		affecteTreeBST(bstFD, FDNodeBST(node));
		size_t n=1;
		n += sizeOfBST(bstFG) + sizeOfBST(bstFD);
		free(bstFG);
		free(bstFD);
		return n;
	}
}

bool insertInBST(BinarySearchTree* bst, const void* key, const void* value){
	NodeBST* node = treeBST(bst);
	NodeBST* nodePrec = NULL;
	while(node!=NULL)
	{
		nodePrec = node;
		if((bst->functionComparison(keyNodeBST(node), key)) == 1)
		{
			node = FGNodeBST(node);
		}
		else
		{
			node = FDNodeBST(node);
		}
	}

	node = creerNodeBST(key, value);
	if(!node)
	{
		return false;
	}
	// insertion dans arbre vide
	if(nodePrec==NULL)
	{
		affecteTreeBST(bst, node);
	}
	else
	{
		if((bst->functionComparison(keyNodeBST(nodePrec), key)) == 1)
		{
			affecteFGNodeBST(nodePrec, node);
		}
		else
		{
			affecteFDNodeBST(nodePrec, node);
		}
	}
	return true;
}

const void* searchBST(BinarySearchTree* bst, const void* key){
	NodeBST* node = treeBST(bst);
	while(node!=NULL && bst->functionComparison(keyNodeBST(node), key) != 0)
	{
		if((bst->functionComparison(keyNodeBST(node), key)) == 1)
		{
			node = FGNodeBST(node);
		}
		else
		{
			node = FDNodeBST(node);
		}
	}
	if(node == NULL)
	{
		return NULL;
	}
	else
	{
		return valueNodeBST(node);
	}
}

LinkedList* getInRange(const BinarySearchTree* bst, void* keyMin, void* keyMax){
	//initialisation d'une pile
	NodeBST** pile = (NodeBST**) malloc(sizeof(NodeBST*)*100);
	if(!pile)
	{
		return NULL;
	}
	int nombreMax = 100, nombreElt = 0;
	LinkedList *list = newLinkedList();
	if(!list)
	{
		free(pile);
		return NULL;
	}
	NodeBST* node = treeBST(bst);
	if(!node)
	{
		free(pile);
		return list;
	}
	else
	{
		//empilement de racine
		pile[nombreElt] = node;
		nombreElt ++;
		node = FGNodeBST(node);
		//parcours infixe iterative en utilisant une pile
		while(node != NULL || nombreElt != 0)
		{
			while(node!=NULL)
			{
				//empiler et verifier si la pile est pleine
				if(nombreElt == nombreMax)
				{
					//reallocation si la pile est pleine
					pile = (NodeBST**) realloc((void*) pile, 2*nombreMax);
					if(!pile)
					{
						freeLinkedList(list, false);
						free(pile);
						return NULL;
					}
					nombreMax *= 2;
				}
				//empilement
				pile[nombreElt++] = node;
				
				node = FGNodeBST(node);
			}
			//depilement
			node = pile[--nombreElt];
			
			//si la cle de noeud est entre keyMin et keyMax alors on insere la valeur de cette clé dans la LinkedList
			if((bst->functionComparison(keyNodeBST(node), keyMin) >= 0) && bst->functionComparison(keyNodeBST(node), keyMax) <= 0)
			{
				bool error = !insertInLinkedList(list, (const void*) valueNodeBST(node));
				if(error)
				{
					free(pile);
					freeLinkedList(list, false);
					return NULL;
				}
			}
			node = FDNodeBST(node);
		} 
	}
	free(pile);
	return list;
}