#include <stdio.h>
#include <stdlib.h>
#include "nodeBST.h"

NodeBST* creerNodeBST(const void* key, const void* value){
	NodeBST *node = (NodeBST*) malloc(sizeof(NodeBST));
	if(!node)
		return NULL;
	affecteKeyNodeBST(node, key);
	affecteValueNodeBST(node, value);
	affecteFGNodeBST(node, NULL);
	affecteFDNodeBST(node, NULL);
	return node;
}

void libererNodeBST(void* node, bool freeContent){
	if(node!=NULL)
	{
		if(freeContent)
		{
			free((void*) valueNodeBST(node));
		}
		free(node);
	}
}

const void*  keyNodeBST(const NodeBST* node){
	return node->key;
}

const void*  valueNodeBST(const NodeBST* node){
	return node->value;
}

NodeBST* FGNodeBST(const NodeBST* node){
	return node->fg;
}

NodeBST* FDNodeBST(const NodeBST* node){
	return node->fd;
}

void affecteKeyNodeBST(NodeBST* node, const void* key){
	node->key = key;
}

void affecteValueNodeBST(NodeBST* node, const void* value){
	node->value = value;
}

void affecteFGNodeBST(NodeBST* node, NodeBST* fg){
	node->fg = fg;
}

void affecteFDNodeBST(NodeBST* node, NodeBST* fd){
	node->fd = fd;
}
