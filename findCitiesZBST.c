#include <stdio.h>
#include <stdlib.h>
#include "findCitiesZBST.h"

double latMin;
double latMax;
double lonMin;
double lonMax;

static int comparaisonUINT64(const void* a, const void* b){
	uint64_t *ptA = (uint64_t *) a, *ptB = (uint64_t *) b;
	if(*ptA > *ptB)
	{
		return 1;
	}
	else if (*ptB > *ptA)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static bool keepIt(const void* ville){
	City* city = (City*) ville;
	if(latitudeCity(city) >= latMin && latitudeCity(city) <= latMax && longitudeCity(city) >= lonMin && longitudeCity(city) <= lonMax){
		return true;
	}
	return false;
}

static void	freeKeysBST(NodeBST* node){
	if(node != NULL)
	{
		freeKeysBST(FGNodeBST(node));
		freeKeysBST(FDNodeBST(node));
		free((void*) keyNodeBST(node));
	}
}

LinkedList* findCities(LinkedList* cities, double latitudeMin, double latitudeMax, double longitudeMin, double longitudeMax){
	latMin = latitudeMin;
	latMax = latitudeMax;
	lonMin = longitudeMin;
	lonMax = longitudeMax;

	//creation d'arbre
	printf("\nCreation d'un arbre avec zcode comme cle...\n");
	BinarySearchTree *citiesBTS = newBST(comparaisonUINT64);
	if (!citiesBTS)
	{
		fprintf(stderr, "Erreur d'allocation memoire pour arbre\n");
		return NULL;
	}
	printf("Arbre est cree.\nReplissage par villes...\n");

	//remplissage d'arbre
	LLNode* llnode = headOfLinkedList(cities);
	int cpt=1;
	while(llnode != NULL)
	{
		City* city = (City*) valueLLNode(llnode);
		uint64_t *zscore = (uint64_t*) malloc(sizeof(uint64_t));
		*zscore = zEncode(latitudeCity(city), longitudeCity(city));
		bool error = !insertInBST(citiesBTS, zscore, city);
		if(error)
		{
			fprintf(stderr, "Erreur d'allocation memoire dans arbre pour la ville numero %d\n", cpt);
			//liberation des cles qui ont été alloué par malloc
			freeKeysBST(treeBST(citiesBTS));
			freeBST(citiesBTS, false);
			return NULL;
		}
		llnode = nextLLNode(llnode);
		cpt++;
	}
	printf("Arbre est rempli.\nFiltration selon zcode dans linked list...\n");

	//filtration selon la cle (zscore)
	uint64_t zcodeMin = zEncode(latitudeMin, longitudeMin), zcodeMax = zEncode(latitudeMax, longitudeMax);
	LinkedList *filtredZscore = getInRange(citiesBTS, &zcodeMin, &zcodeMax);
	if(!filtredZscore)
	{
		fprintf(stderr, "Erreur de filtration selon zscore\n");
		//liberation des cles qui ont été alloué par malloc
		freeKeysBST(treeBST(citiesBTS));
		freeBST(citiesBTS, false);
		return NULL;
	}
	printf("Les villes sont filtrees dans linked list.\nLiberation de memoire alloue par arbre...\n");
	freeKeysBST(treeBST(citiesBTS));
	freeBST(citiesBTS, false);
	printf("L'espace memoire est libere.\nFiltration selon latitude et longitude dans nouveau linked list...\n");

	//filtration selon latitude et longitude
	LinkedList *filtred = filterLinkedList(filtredZscore, keepIt);

	printf("Les villes sont filtrees selon latitude et longitude.\nLiberation de memoire alloue par ancien linked list...\n");
	freeLinkedList(filtredZscore, false);
	printf("L'espace memoire est libere.\n");
	return filtred;
}