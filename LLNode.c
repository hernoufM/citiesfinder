#include <stdio.h>
#include <stdlib.h>
#include "LLNode.h"

LLNode* creerLLNode(const void* value, LLNode* nextNode){
	LLNode* lln=(LLNode*) malloc(sizeof(LLNode));
	if(!lln)
		return NULL;
	affecteValueLLNode(lln, value);
	affecteNextLLNode(lln, nextNode);
	return lln;
}

const void* valueLLNode(LLNode* node){
	return node->value;
}

LLNode* nextLLNode(LLNode* node){
	return node->next;
}

void affecteValueLLNode(LLNode* node, const void * value){
	node->value = value;
}

void affecteNextLLNode(LLNode* node, LLNode* nextNode){
	node->next = nextNode;
}

void libererLLNode(LLNode* node, bool freeContent){
	if(node!=NULL)
	{
		if(freeContent)
		{
			free((void*) valueLLNode(node));	// Discard const qualifier
		}
		free(node);
	}
}
