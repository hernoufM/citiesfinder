#include <stdio.h>
#include <stdlib.h>
#include "findCitiesKDBST.h"

double latMin;
double latMax;
double lonMin;
double lonMax;

static int comparaisonDouble(const void* a, const void* b){
	double *ptA = (double*) a, *ptB = (double*) b;
	if(*ptA>*ptB)
	{
		return 1;
	}
	else if(*ptB>*ptA)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static bool keepIt(const void* ville){
	City* city = (City*) ville;
	if(latitudeCity(city) >= latMin && latitudeCity(city) <= latMax && longitudeCity(city) >= lonMin && longitudeCity(city) <= lonMax){
		return true;
	}
	return false;
}

LinkedList* findCities(LinkedList* cities, double latitudeMin, double latitudeMax, double longitudeMin, double longitudeMax){
	latMin = latitudeMin;
	latMax = latitudeMax;
	lonMin = longitudeMin;
	lonMax = longitudeMax;

	//creation d'arbre
	printf("\nCreation d'un kd-arbre avec latitude et longitude comme cle...\n");
	int (* tabFonctionsComparaisons [2] ) (const void* a, const void* b);
	tabFonctionsComparaisons[0] = comparaisonDouble;
	tabFonctionsComparaisons[1] = comparaisonDouble;
	kdBinarySearchTree *citiesBTS = newkdBST(2, tabFonctionsComparaisons);
	if (!citiesBTS)
	{
		fprintf(stderr, "Erreur d'allocation memoire pour arbre\n");
		return NULL;
	}
	printf("Arbre est cree.\nReplissage par villes...\n");

	//remplissage d'arbre
	LLNode* llnode = headOfLinkedList(cities);
	int cpt=1;
	while(llnode != NULL)
	{
		City* city = (City*) valueLLNode(llnode);
		double* tabKeys [2];
		tabKeys[0] = &(city->latitude);
		tabKeys[1] = &(city->longitude);
		bool error = !insertInkdBST(citiesBTS, (const void**) tabKeys, city);
		if(error)
		{
			fprintf(stderr, "Erreur d'allocation memoire dans arbre pour la ville numero %d\n", cpt);
			freekdBST(citiesBTS, false);
			return NULL;
		}
		llnode = nextLLNode(llnode);
		cpt++;
	}
	printf("Arbre est rempli.\nFiltration...\n");

	//filtration. On va garder les villes qui sont soit entre deux latitude soit entre deux longitude
	double* tabKeysMin [2];
	tabKeysMin [0] = &latitudeMin;
	tabKeysMin [1] = &longitudeMin;
	double* tabKeysMax [2];
	tabKeysMax [0] = &latitudeMax;
	tabKeysMax [1] = &longitudeMax;
	LinkedList *filtred = getkdInRange(citiesBTS, (void**) tabKeysMin, (void**) tabKeysMax);
	if(!filtred)
	{
		fprintf(stderr, "Erreur de filtration\n");
		freekdBST(citiesBTS, false);
		return NULL;
	}
	printf("Les villes sont filtrees dans linked list.\nLiberation de memoire alloue par arbre...\n");
	freekdBST(citiesBTS, false);
	printf("L'espace memoire est libere.\nFiltration des villes etre deux latitudes et deux longitudes...\n");

	//Filtration des ville. On va garder que des bonnes villes.
	filtred = filterLinkedList(filtred, keepIt);
	return filtred;
}