#include <stdio.h>
#include <stdlib.h>
#include "kdBinarySearchTree.h"

kdBinarySearchTree* newkdBST(int nombreDimensions, int (**comparison_fn_t)(const void *, const void *)){
	kdBinarySearchTree* bst = (kdBinarySearchTree*) malloc(sizeof(kdBinarySearchTree));
	if(!bst)
		return NULL;
	affecteNombreDimensionkdBST(bst, nombreDimensions);
	affecteTreekdBST(bst, NULL);
	bst->tabFunctionComparison = comparison_fn_t;
	return bst;
}

void freekdBST(kdBinarySearchTree* bst, bool freeContent){
	if(bst)
	{
		kdNodeBST* node = treekdBST(bst);
		if(node)
		{
			kdBinarySearchTree* bstFG = newkdBST(nombreDimensionskdBST(bst), bst->tabFunctionComparison), *bstFD = newkdBST(nombreDimensionskdBST(bst), bst->tabFunctionComparison);
			affecteTreekdBST(bstFG, FGkdNodeBST(node));
			affecteTreekdBST(bstFD, FDkdNodeBST(node));
			freekdBST(bstFG, freeContent);
			freekdBST(bstFD, freeContent);
			libererkdNodeBST(treekdBST(bst), freeContent);
		}
		free(bst);
	}
}

int nombreDimensionskdBST(const kdBinarySearchTree* bst){
	return bst->nombreDimensions;
}

kdNodeBST* treekdBST(const kdBinarySearchTree* bst){
	return bst->tree;
}

void affecteNombreDimensionkdBST(kdBinarySearchTree* bst, int nombreDimensions){
	bst->nombreDimensions = nombreDimensions;
}

void affecteTreekdBST(kdBinarySearchTree* bst, kdNodeBST* node){
	bst->tree = node;
}

size_t sizeOfkdBST(const kdBinarySearchTree* bst){
	kdNodeBST* node = treekdBST(bst);
	if(!node)
	{
		return 0;
	}
	else
	{
		kdBinarySearchTree *bstFG = newkdBST(nombreDimensionskdBST(bst), bst->tabFunctionComparison), *bstFD = newkdBST(nombreDimensionskdBST(bst), bst->tabFunctionComparison);
		affecteTreekdBST(bstFG, FGkdNodeBST(node));
		affecteTreekdBST(bstFD, FDkdNodeBST(node));
		size_t n=1;
		n += sizeOfkdBST(bstFG) + sizeOfkdBST(bstFD);
		free(bstFG);
		free(bstFD);
		return n;
	}
}

bool insertInkdBST(kdBinarySearchTree* bst, const void** keys, const void* value){
	kdNodeBST* node = treekdBST(bst);
	kdNodeBST* nodePrec = NULL;
	while(node!=NULL)
	{
		nodePrec = node;
		if((bst->tabFunctionComparison[dimensionkdNodeBST(node)](keykdNodeBST(node), keys[dimensionkdNodeBST(node)])) == 1)
		{
			node = FGkdNodeBST(node);
		}
		else
		{
			node = FDkdNodeBST(node);
		}
	}

	if(nodePrec == NULL)
	{
		node = creerkdNodeBST(0, keys[0], value);
	}
	else
	{
		node = creerkdNodeBST((dimensionkdNodeBST(nodePrec) == nombreDimensionskdBST(bst)-1)? 0 : dimensionkdNodeBST(nodePrec)+1, keys[(dimensionkdNodeBST(nodePrec) == nombreDimensionskdBST(bst)-1)? 0 : dimensionkdNodeBST(nodePrec)+1], value);
	}

	if(!node)
	{
		return false;
	}
	// insertion dans arbre vide
	if(nodePrec==NULL)
	{
		affecteTreekdBST(bst, node);
	}
	else
	{
		if((bst->tabFunctionComparison[dimensionkdNodeBST(nodePrec)](keykdNodeBST(nodePrec), keys[dimensionkdNodeBST(nodePrec)])) == 1)
		{
			affecteFGkdNodeBST(nodePrec, node);
		}
		else
		{
			affecteFDkdNodeBST(nodePrec, node);
		}
	}
	return true;
}

//const void* searchkdBST(kdBinarySearchTree* bst, const void* key);
LinkedList* getkdInRange(const kdBinarySearchTree* bst, void** keysMin, void** keysMax){
	//initialisation d'une pile
	kdNodeBST** pile = (kdNodeBST**) malloc(sizeof(kdNodeBST*)*100);
	if(!pile)
	{
		return NULL;
	}
	int nombreMax = 100, nombreElt = 0;
	LinkedList *list = newLinkedList();
	if(!list)
	{
		free(pile);
		return NULL;
	}
	kdNodeBST* node = treekdBST(bst);
	if(!node)
	{
		free(pile);
		return list;
	}
	else
	{
		//empilement de racine
		pile[nombreElt] = node;
		nombreElt ++;
		node = FGkdNodeBST(node);
		//parcours infixe iterative en utilisant une pile
		while(node != NULL || nombreElt != 0)
		{
			while(node!=NULL)
			{
				//empiler et verifier si la pile est pleine
				if(nombreElt == nombreMax)
				{
					//reallocation si la pile est pleine
					pile = (kdNodeBST**) realloc((void*) pile, 2*nombreMax);
					if(!pile)
					{
						freeLinkedList(list, false);
						free(pile);
						return NULL;
					}
					nombreMax *= 2;
				}
				//empilement
				pile[nombreElt++] = node;
				
				node = FGkdNodeBST(node);
			}
			//depilement
			node = pile[--nombreElt];
			
			if((bst->tabFunctionComparison[dimensionkdNodeBST(node)](keykdNodeBST(node), keysMin[dimensionkdNodeBST(node)]) >= 0) && bst->tabFunctionComparison[dimensionkdNodeBST(node)](keykdNodeBST(node), keysMax[dimensionkdNodeBST(node)]) <= 0)
			{
				bool error = !insertInLinkedList(list, (const void*) valuekdNodeBST(node));
				if(error)
				{
					free(pile);
					freeLinkedList(list, false);
					return NULL;
				}
			}
			node = FDkdNodeBST(node);
		} 
	}
	return list;
}