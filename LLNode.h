#ifndef INCLUDE_LLNODE_H
#define INCLUDE_LLNODE_H

#include <stddef.h>
#include <stdbool.h>

typedef struct llnode_t {
    const void* value;
    struct llnode_t* next;
} LLNode;

LLNode* creerLLNode(const void* value, LLNode* nextNode);
const void* valueLLNode(LLNode* node);
LLNode* nextLLNode(LLNode* node);
void affecteValueLLNode(LLNode* node, const void* value);
void affecteNextLLNode(LLNode* node, LLNode* nextNode);
void libererLLNode(LLNode* node, bool freeContent);

#endif