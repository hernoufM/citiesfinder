#ifndef _KD_NODE_BST_H_
#define _KD_NODE_BST_H_

#include <stdbool.h>

typedef struct kdNodeBST_t {
	int dimension;
	const void* key;
	const void* value;
	struct kdNodeBST_t* fg;
	struct kdNodeBST_t* fd;
} kdNodeBST;

kdNodeBST* creerkdNodeBST(int, const void*, const void*);
void libererkdNodeBST(void* , bool);
int dimensionkdNodeBST(const kdNodeBST*);
const void*  keykdNodeBST(const kdNodeBST*);
const void*  valuekdNodeBST(const kdNodeBST*);
kdNodeBST* FGkdNodeBST(const kdNodeBST*);
kdNodeBST* FDkdNodeBST(const kdNodeBST*);
void affecteDimensionkdNodeBST(kdNodeBST*, int);
void affecteKeykdNodeBST(kdNodeBST*, const void*);
void affecteValuekdNodeBST(kdNodeBST*, const void*);
void affecteFGkdNodeBST(kdNodeBST*, kdNodeBST*);
void affecteFDkdNodeBST(kdNodeBST*, kdNodeBST*);

#endif