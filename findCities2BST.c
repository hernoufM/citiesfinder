#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "findCities2BST.h"

static int comparaisonCity(const void* x, const void* y){
	City *city1 = (City*) x, *city2 = (City*) y;
	if(strcmp(nameCity(city1), nameCity(city2)) == 0)
	{
		if(latitudeCity(city1) == latitudeCity(city2))
		{
			if(longitudeCity(city1) == longitudeCity(city2))
			{
				return 0;
			}
			else if(longitudeCity(city1) < longitudeCity(city2))
			{
				return -1;
			}
			else 
			{
				return 1;
			}
		}
		else if(latitudeCity(city1) < latitudeCity(city2))
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	else if(strcmp(nameCity(city1), nameCity(city2)) == -1)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

static int comparaisonDouble(const void* a, const void* b){
	double *ptA = (double*) a, *ptB = (double*) b;
	if(*ptA>*ptB)
	{
		return 1;
	}
	else if(*ptB>*ptA)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

LinkedList* findCities(LinkedList* cities, double latitudeMin, double latitudeMax, double longitudeMin, double longitudeMax){
	//creation des arbres
	printf("\nCreation des arbres avec latitude et longitude comme cles...\n");
	BinarySearchTree *citiesLatitudeBTS = newBST(comparaisonDouble);
	if (!citiesLatitudeBTS)
	{
		fprintf(stderr, "Erreur d'allocation memoire pour arbre de latitude\n");
		return NULL;
	}
	BinarySearchTree *citiesLongitudeBTS = newBST(comparaisonDouble);
	if (!citiesLongitudeBTS)
	{
		fprintf(stderr, "Erreur d'allocation memoire pour arbre de longitude\n");
		return NULL;
	}
	printf("Les arbres sont crees.\nReplissage d'arbre avec latitude comme cle par villes...\n");

	//remplissage d'arbres de latitude
	LLNode* llnode = headOfLinkedList(cities);
	int cpt=1;
	while(llnode != NULL)
	{
		City* city = (City*) valueLLNode(llnode);
		bool error = !insertInBST(citiesLatitudeBTS, &(city->latitude), city);
		if(error)
		{
			fprintf(stderr, "Erreur d'allocation memoire dans arbre de latitude pour la ville numero %d\n", cpt);
			freeBST(citiesLatitudeBTS, false);
			freeBST(citiesLongitudeBTS, false);
			return NULL;
		}
		llnode = nextLLNode(llnode);
		cpt++;
	}
	printf("Arbre est rempli.\nReplissage d'arbre avec longitude comme cle par villes...\n");

	//remplissage d'arbres de longitude
	llnode = headOfLinkedList(cities);
	cpt=1;
	while(llnode != NULL)
	{
		City* city = (City*) valueLLNode(llnode);
		bool error = !insertInBST(citiesLongitudeBTS, &(city->longitude), city);
		if(error)
		{
			fprintf(stderr, "Erreur d'allocation memoire dans arbre de longitude pour la ville numero %d\n", cpt);
			freeBST(citiesLongitudeBTS, false);
			freeBST(citiesLatitudeBTS, false);
			return NULL;
		}
		llnode = nextLLNode(llnode);
		cpt++;
	}
	printf("Arbre est rempli.\nFiltration selon latitude dans linked list...\n");

	//filtration selon la cle (latitude)
	LinkedList *filtredLatitude = getInRange(citiesLatitudeBTS, &latitudeMin, &latitudeMax);
	if(!filtredLatitude)
	{
		fprintf(stderr, "Erreur de filtration selon latitude\n");
		freeBST(citiesLongitudeBTS, false);
		freeBST(citiesLatitudeBTS, false);
		return NULL;
	}
	printf("Les villes sont filtrees dans linked list de latitude.\nLiberation de memoire alloue par arbre avec latitude comme cle...\n");
	freeBST(citiesLatitudeBTS, false);
	printf("L'espace memoire est libere.\nFiltration selon longitude dans linked list...\n");

	//filtration selonle cle (longitude)
	LinkedList *filtredLongitude = getInRange(citiesLongitudeBTS, &longitudeMin, &longitudeMax);
	if(!filtredLongitude)
	{
		fprintf(stderr, "Erreur de filtration selon longitude\n");
		freeBST(citiesLongitudeBTS, false);
		freeBST(citiesLatitudeBTS, false);
		return NULL;
	}
	printf("Les villes sont filtrees dans linked list de longitude.\nLiberation de memoire alloue par arbre avec longitude comme cle...\n");
	freeBST(citiesLongitudeBTS, false);
	printf("L'espace memoire est libere.\nIntersection de deux linked list...\n");

	//intersection de deux linked list
	LinkedList* intersection = intersect(filtredLatitude, filtredLongitude, comparaisonCity);
	if(!intersection)
	{
		freeLinkedList(filtredLatitude, false);
		freeLinkedList(filtredLongitude, false);
		return NULL;
	}
	printf("L'intersection est terminee.\nLiberation memoire alloues par linked lists\n");
	freeLinkedList(filtredLatitude, false);
	freeLinkedList(filtredLongitude, false);
	printf("L'espace memoire est libere.\n");
	return intersection;
}