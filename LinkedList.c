/* ========================================================================= *
 * LinkedList definition
 * ========================================================================= */


#include <stddef.h>
#include <stdlib.h>
#include "LinkedList.h"

LinkedList* newLinkedList(void)
{
    LinkedList* ll = (LinkedList*) malloc(sizeof(LinkedList));
    if (!ll)
        return NULL;
    ll->head = NULL;
    ll->last = NULL;
    ll->size = 0;
    return ll;
}

void freeLinkedList(LinkedList* ll, bool freeContent)
{
    // Free LLNodes
    LLNode* node = headOfLinkedList(ll);
    LLNode* prev = NULL;
    while(node != NULL)
    {
        prev = node;
        node = nextLLNode(node);
        libererLLNode(prev, freeContent);
    }
    // Free LinkedList sentinel
    free(ll);
}

LLNode* headOfLinkedList(const LinkedList* ll){
    return ll->head;
}

LLNode* lastOfLinkedList(const LinkedList* ll){
    return ll->last;
}

size_t sizeOfLinkedList(const LinkedList* ll)
{
    return ll->size;
}

bool insertInLinkedList(LinkedList* ll, const void* value)
{
    // Initialisation
    LLNode* node = creerLLNode(value, NULL);
    if(!node)
        return false;
    // Adding the node to the list
    if(!lastOfLinkedList(ll))
    {
        // First element in the list
        ll->last = node;
        ll->head = node;
    } else {
        //At least one element in the list
        affecteNextLLNode(lastOfLinkedList(ll), node);
        ll->last = node;
    }
    // In both cases, increment size
    ll->size++;
    return true;
}

LinkedList* filterLinkedList(LinkedList* ll,  bool keepIt_fn_t(const void*))
{
    LinkedList* filtered = newLinkedList();
    if (!filtered)
        return NULL;
    LLNode* curr = headOfLinkedList(ll);
    bool error = false;
    while(!error && curr != NULL)
    {
        if(keepIt_fn_t(valueLLNode(curr)))
            error = !insertInLinkedList(filtered, valueLLNode(curr));
        curr = nextLLNode(curr);
    }
    if(error)
    {
        freeLinkedList(filtered, false);
        return NULL;
    }
    return filtered;
}