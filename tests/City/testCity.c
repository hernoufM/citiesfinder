#include <stdio.h>
#include <stdlib.h>
#include "../../City.h"

int main(int argc, char *argv[])
{
	if(argc!=4)
	{
		printf("Donnez nom de la ville, sa latitude et longitude\n");
		exit(0);
	}
	char *nom = argv[1];
	double latitude = strtod(argv[2], NULL);
	double longitude = strtod(argv[3], NULL);
	City* city = creerCity(nom, latitude, longitude);

	printf("Nom = %s\nLatitude = %.5lf\nLongitude = %.5lf\n", nameCity(city), latitudeCity(city), longitudeCity(city));
	libererCity(city);
	return 0;
}