#include <stdio.h>
#include <stdlib.h>
#include "../../findCities.h"
#include "../../LinkedList.h"
int main(int argc, char *argv[])
{
	LinkedList *cities = newLinkedList();

	City* city = creerCity("Lissabonne",-5.0,-5.0);
	insertInLinkedList(cities,city);

	city = creerCity("Barcelone",-5.0,-4.0);
	insertInLinkedList(cities,city);

	city = creerCity("Madrid",-4.0,-4.0);
	insertInLinkedList(cities,city);

	city = creerCity("Marselle",-3.0,-3.0);
	insertInLinkedList(cities,city);

	city = creerCity("Paris",-1.0,-3.0);
	insertInLinkedList(cities,city);

	city = creerCity("London",2.0,-3.0);
	insertInLinkedList(cities,city);

	city = creerCity("Berlin",-1.0,-1.0);
	insertInLinkedList(cities,city);

	city = creerCity("Amsterdam",0.0,-2.0);
	insertInLinkedList(cities,city);

	city = creerCity("Rome",-4.0, 0.0);
	insertInLinkedList(cities,city);

	city = creerCity("Oslo", 4.0, 4.0);
	insertInLinkedList(cities,city);

	city = creerCity("Minsk", 0.0, 4.0);
	insertInLinkedList(cities,city);

	city = creerCity("Kiev", -1.0, 4.0);
	insertInLinkedList(cities,city);

	city = creerCity("Moscow", 1.0, 5.0);
	insertInLinkedList(cities,city);

	LinkedList *newCities = findCities(cities,-3.0, 3.0, -3.0, 4.0);
	LLNode* curr = headOfLinkedList(newCities);
	printf("Les villes restantes :\n");
	while(curr!=NULL)
	{
		printf("%s\n", nameCity((City*) valueLLNode(curr)));
		curr=nextLLNode(curr);
	}
	freeLinkedList(cities, true);
	freeLinkedList(newCities, true);
	return 0;
}