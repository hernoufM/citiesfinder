#include <stdlib.h>
#include <stdio.h>
#include "../../zscore.h"

int main(int argc, char const *argv[])
{
	if(argc!=3)
	{
		fprintf(stderr, "Donnez latitude et longitude.\n");
		exit(EXIT_FAILURE);
	}
	double latitude = strtod(argv[1], NULL);
	double longitude = strtod(argv[2], NULL);
	uint64_t code = zEncode(latitude, longitude);
	printf("%lu\n", code);

	return 0;
}