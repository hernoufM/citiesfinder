#include <stdio.h>
#include <stdlib.h>
#include "../../LLNode.h"

int main(int argc, char const *argv[])
{
	int x, *ptX=&x, *d, *ptY;
	x=74;
	LLNode* node = creerLLNode(ptX, NULL);

	char chaine[50]="SALUT";
	LLNode* firstNode= creerLLNode(chaine, node);

	char chaine2[50]="AU REVOIR";
	LLNode* lastNode= creerLLNode(chaine2,NULL);
	affecteNextLLNode(node, lastNode);

	printf("On est dans premier node qui est dans adresse %p\n",firstNode);
	printf("Sa valeur est chaine du caractere %s\n",(char *) valueLLNode(firstNode));
	printf("Son suivant se trouvent à adresse %p\n\n",nextLLNode(firstNode));
	printf("On est dans deuxieme node qui est dans adresse %p\n",node);
	d=(int*) valueLLNode(node);
	*d=10;
	printf("Sa valeur est entier qui a adresse %p et sa valeur est %d\n",d, *d);
	int y=100;
	ptY = &y;
	affecteValueLLNode(node, ptY);
	d = (int*) valueLLNode(node);
	printf("Sa valeur maintenant est entier qui a adresse %p et sa valeur est %d\n",d, *d);
	printf("Son suivant se trouvent à adresse %p\n\n",nextLLNode(node));
	printf("On est dans troisieme node qui est dans adresse %p\n",lastNode);
	printf("Sa valeur est chaine du caractere %s\n",(char *) valueLLNode(lastNode));
	printf("Son suivant se trouvent à adresse %p\n\n",nextLLNode(lastNode));
	libererLLNode(lastNode,false);

	
	return 0;
}
