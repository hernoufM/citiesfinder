#include <stdio.h>
#include <stdlib.h>
#include "../../BinarySearchTree.h"

int comparaisonInt(const void* a, const void* b){
	int* ptA = (int*) a;
	int* ptB = (int*) b;
	if(*ptA>*ptB)
	{
		return 1;
	}
	else if(*ptB>*ptA)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int main(int argc, char const *argv[])
{
	BinarySearchTree *bst = newBST(comparaisonInt);
	printf("Adresse memoire de BST = %p\n", bst);
	printf("Adresse memoire de fonction de comparaison de cette BST = %p\n", bst->functionComparison);
	printf("Pour l'instant adresse memoire de arbre de BST = %p\n", treeBST(bst));

	int x = 1, *ptX = &x;
	char chaine [10] = "Salut"; 
	NodeBST* node = creerNodeBST(ptX, chaine);
	affecteTreeBST(bst, node);
	printf("On a creer une racine. Maintenant adresse memoire de arbre de BST = %p\n", treeBST(bst));
	int y = 2, *ptY = &y;
	char chaine2 [15] = "Je suis Mohamed";
	insertInBST(bst, ptY, chaine2);
	int z = 0, *ptZ = &z;
	char chaine3 [10] = "Bonjour";
	insertInBST(bst, ptZ, chaine3);
	int j = 5, *ptJ = &j;
	char chaine4 [10] = "Au revoir";
	insertInBST(bst, ptJ, chaine4);
	int l = 4, *ptL = &l;
	char chaine5 [10] = "Hein";
	insertInBST(bst, ptL, chaine5);

	printf("Le bst a ete modifie. Le size est = %lu\n", sizeOfBST(bst));
	printf("Voici les elements de arbre:\n");
	printf("%s\n", (char*) valueNodeBST(FGNodeBST(treeBST(bst))));
	printf("%s\n", (char*) valueNodeBST(treeBST(bst)));
	printf("%s\n", (char*) valueNodeBST(FDNodeBST(treeBST(bst))));
	printf("%s\n", (char*) valueNodeBST(FDNodeBST(FDNodeBST(treeBST(bst)))));	

	int h = 5, *ptH = &h;
	printf("La chaine dont cle est %d est = %s\n", h, (char*) searchBST(bst, ptH));
	int a = 0, *ptA = &a, b = 5, *ptB=&b;
	LinkedList* list = getInRange(bst, ptA, ptB);
	printf("%p\n", list);
	printf("On a filtrer bst dans liste pour garder que les cles entre %d et %d\n", a, b);
	printf("La taille de liste est %lu\n", sizeOfLinkedList(list));
	printf("La valeur de tete est %s\n", (char*) valueLLNode(headOfLinkedList(list)));
	freeLinkedList(list, false);
	freeBST(bst, false);
	return 0;
}