#include <stdio.h>
#include <stdlib.h>
#include "../../LinkedList.h"

bool estAvantD(const void* chaine);

int main(int argc, char const *argv[])
{
	LinkedList* ll=newLinkedList();
	char chaine1 [50]="Salut";
	printf("La nouvelle Linked List a ete cree dans adresse %p\nSa tete pointe vers %p\nSa queue pointe vers %p\nEt sa size est %lu\n",
		ll, headOfLinkedList(ll), lastOfLinkedList(ll), sizeOfLinkedList(ll));
	insertInLinkedList(ll, chaine1);

	char chaine2[50]="Comment alez vouz?";
	insertInLinkedList(ll, chaine2);

	char chaine3[50]="Bon Journee";
	insertInLinkedList(ll,chaine3);

	char chaine4 [50]="Au revoir";
	insertInLinkedList(ll,chaine4);

	printf("La nouvelle Linked List a ete renouvle\nValue de prochaine node de sa tete %s\nSa queue est %s\nEt sa size est %lu\n",
		(char *) valueLLNode(nextLLNode(headOfLinkedList(ll))), (char *) valueLLNode(lastOfLinkedList(ll)), sizeOfLinkedList(ll));

	LinkedList *filtred = filterLinkedList(ll, estAvantD);
	printf("On a filtre la list. Le premier valeur maintenant est %s\nEt sa size est %lu\n",(char *) valueLLNode(headOfLinkedList(filtred)), sizeOfLinkedList(filtred));  

	return 0;
}


bool estAvantD(const void* chaine) {
	const char* string = chaine;
	if(*string < 'D')
	{
		return true;
	} 
	return false;
}
