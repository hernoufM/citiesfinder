#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "../../findCitiesZBST.h"

// Max length of a single line in a .csv file
static const size_t LINE_SIZE = 512;




/* ------------------------------------------------------------------------- *
 * Parse a CSV file containing cities.
 * This CSV must start with a header row and have three columns:
 *   1) Name: the name of the city (delimited with double quotes)
 *   2) Latitude: the latitude of the city in degree (floating-point value)
 *   3) Longitude: the longitude of the city in degree (floating-point value)
 *
 * PARAMETERS
 * filename     A null-terminated string containing the name of the CSV file
 *
 * RETURN
 * cities       A linked list containing the cities
 * ------------------------------------------------------------------------- */
static LinkedList* parseCsv(const char* filename)
{
    // Opens the file
    FILE* fileObj = fopen(filename, "r");
    if (fileObj == NULL)
    {
        fprintf(stderr, "Erreur lors ouverture du fichier '%s'. Fin d'execution...\n", filename);
        exit(EXIT_FAILURE);
    }
    LinkedList* cities = newLinkedList();
    if (!cities)
    {
        fprintf(stderr, "Erreur lors d'allocation memoire. Fin d'execution...\n");
        exit(EXIT_FAILURE);
    }

    // Start reading the file, line by line (N.B.: fgets() reads until next \n or EOF character)
    char line[LINE_SIZE];
    size_t nbLine = 0;
    size_t nbCity = 0;
    char delim = ',';
    char stringDelim = '"';
    City* city;
    size_t currChar, nameStart, nameEnd, nameLen, latitudeStart, longitudeStart;
    while (fgets(line, sizeof(line), fileObj) != NULL)
    {
        if (nbLine == 0) { nbLine++; continue; } // Skip header

        char* nomCity = NULL;
        double latitudeCity = 0.0, longitudeCity = 0.0;
        currChar = 0;

        // Skip anything before the first string delimiter of the city name
        while (line[currChar++] != stringDelim) { }

        nameStart = currChar;

        // Find position of the end of the city name
        while (line[currChar] != stringDelim) { currChar++; }

        nameEnd = currChar, nameLen = nameEnd - nameStart; // Index one past the end of the name

        // City name
        nomCity = malloc(sizeof(char) * (nameLen + 1));
        if (!nomCity) {
            fprintf(stderr, "Erreur lors d'allocation memoire : le nom de la ville à la ligne %zu. Fin d'execution...\n", nbLine);
            exit(EXIT_FAILURE);
        }
        memcpy(nomCity, line + nameStart, sizeof(char) * nameLen);
        nomCity[nameLen] = '\0';

        // Skip until the latitude start
        while (line[currChar++] != delim) { }

        latitudeStart = currChar;

        // Latitude
        latitudeCity = strtod(line + latitudeStart, NULL);

        // Skip until delimiter
        while (line[currChar++] != delim) { }

        longitudeStart = currChar;

        // Longitude
        longitudeCity = strtod(line + longitudeStart, NULL);

        city = creerCity(nomCity, latitudeCity, longitudeCity);
        if(!city)
        {
            fprintf(stderr, "Erreur lors d'allocation memoire %zu. Fin d'execution...\n", nbLine);
            exit(EXIT_FAILURE);
        }

        if(!insertInLinkedList(cities, (const void*)city))
        {
            fprintf(stderr, "Erreur lors d'allocation memoire: insertion à la ligne %zu. Fin d'execution...\n", nbLine);
            exit(EXIT_FAILURE);
        }

        nbLine++;
        nbCity++;
    }
    fclose(fileObj);
    return cities;
}



int main(int argc, char** argv)
{
    // Parse command line input
    if (argc  != 6)
    {
        fprintf(stderr, "%s attend exactement 5 parametres :\n"
                        " (1) un fichier .csv qui contient tous les villes avec leur coordonnees\n"
                        " (2) premiere latitude\n (3) premiere longitude\n"
                        " (4) deuxieme latitude\n (5) deuxieme longitude\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    char fileName [100] = "../../BD/";
    strcat(fileName, argv[1]);
    double latitudeA = strtod(argv[2], NULL);
    double longitudeA = strtod(argv[3], NULL);
    double latitudeB = strtod(argv[4], NULL);
    double longitudeB = strtod(argv[5], NULL);
    // Compute min/max latitude/longitude
    double latitudeMin = (latitudeA < latitudeB)? latitudeA: latitudeB;
    double latitudeMax = (latitudeA < latitudeB)? latitudeB: latitudeA;
    double longitudeMin = (longitudeA < longitudeB)? longitudeA: longitudeB;
    double longitudeMax = (longitudeA < longitudeB)? longitudeB: longitudeA;

    // Parse csv file
    LinkedList* cities = parseCsv(fileName);
    size_t nbCities = sizeOfLinkedList(cities);
    printf("Nombre des villes: %lu\n", nbCities);

    // Compute the cities in the box
    LinkedList* citiesInBox = findCities(cities, latitudeMin, latitudeMax,
                                         longitudeMin, longitudeMax);
    if(!citiesInBox)
    {
        fprintf(stderr, "Erreur lors d'allocation memoire lors de recherche des villes. Fin d'execution...\n");
        exit(EXIT_FAILURE);
    }

    // Print stuff
    nbCities = sizeOfLinkedList(citiesInBox);
    printf("Nombre des villes apres le filtration: %lu\n", nbCities);
    if(nbCities <= 50)
    {
        LLNode* node = headOfLinkedList(citiesInBox);
        City* city;
        while(node != NULL)
        {
            city = (City*) valueLLNode(node);
            printf("%s (%f, %f)\n", nameCity(city), latitudeCity(city), longitudeCity(city));
            node = nextLLNode(node);
        }
    }

    // Free the linked list: the name must be freed beforehand
    freeLinkedList(citiesInBox, false);
    LLNode* node = headOfLinkedList(cities);
    City* city;
    while(node != NULL)
    {
        city = (City*) valueLLNode(node);
        libererCity(city);
        node = nextLLNode(node);
    }
    freeLinkedList(cities, false);

    exit(EXIT_SUCCESS);
}