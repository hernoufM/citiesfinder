#include <stdio.h>
#include <stdlib.h>
#include "../../nodeBST.h"

int main(int argc, char const *argv[])
{
	int tab [5] = {1,2,3,4,5};
	char * tabChaine [5] = {"J'aime mes freres. ", "J'aime mes parents. ", "J'aime Ksyousha. ", "J'aime mes grand-parents. ", "J'aime ma famille.\n"};
	NodeBST* node1 = creerNodeBST(tab, tabChaine[0]);
	NodeBST* node2 = creerNodeBST(tab+1, tabChaine[1]);
    NodeBST* node3 = creerNodeBST(tab+2, tabChaine[2]);
    NodeBST* node4 = creerNodeBST(tab+3, tabChaine[3]);
	NodeBST* node5 = creerNodeBST(tab+4, tabChaine[4]);

	printf("Test de keyNodeBST et valueNodeBST\n");
	int* num = (int *) keyNodeBST(node1);
	printf("key de premier node est %d, value de premier node est %s\n", *num, (char *) valueNodeBST(node1));
	num = (int *) keyNodeBST(node2);
	printf("key de deuxieme node est %d, value de deuxieme node est %s\n", *num, (char *) valueNodeBST(node2));
	
	printf("Test de affecteKeyNodeBST et affecteValueNodeBST\n");
	affecteKeyNodeBST(node1, tab+1);
	affecteKeyNodeBST(node2, tab);
	affecteValueNodeBST(node1, tabChaine[1]);
	affecteValueNodeBST(node2, tabChaine[0]);
	num = (int *) keyNodeBST(node1);
	printf("key de premier node est %d, value de premier node est %s\n", *num, (char *) valueNodeBST(node1));
	num = (int *) keyNodeBST(node2);
	printf("key de deuxieme node est %d, value de deuxieme node est %s\n", *num, (char *) valueNodeBST(node2));

	printf("Test de affecteFGNodeBST, affecteFDNodeBST, \n");
	affecteFGNodeBST(node3, node2);
	affecteFGNodeBST(node2, node1);
	affecteFDNodeBST(node3, node4);
	affecteFDNodeBST(node4, node5);
	printf("%s%s%s%s%s\n", (char*) valueNodeBST(FGNodeBST(FGNodeBST(node3))), (char*) valueNodeBST(FGNodeBST(node3)), (char*) valueNodeBST(node3), (char*) valueNodeBST(FDNodeBST(node3)), (char*) valueNodeBST(FDNodeBST(FDNodeBST(node3))));

	libererNodeBST(node1, false);
	libererNodeBST(node2, false);
	libererNodeBST(node3, false);
	libererNodeBST(node4, false);
	libererNodeBST(node5, false);
	return 0;
}