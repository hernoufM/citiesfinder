#ifndef _FIND_CITIES_KDBST_H
#define _FIND_CITIES_KDBST_H

#include "LinkedList.h"
#include "kdBinarySearchTree.h"
#include "City.h"

LinkedList* findCities(LinkedList*, double, double, double, double);

#endif