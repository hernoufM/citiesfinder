#ifndef _FIND_CITIES_ZBST_H
#define _FIND_CITIES_ZBST_H

#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "City.h"
#include "zscore.h"

LinkedList* findCities(LinkedList*, double, double, double, double);

#endif