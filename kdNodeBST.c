#include <stdio.h>
#include <stdlib.h>
#include "kdNodeBST.h"

kdNodeBST* creerkdNodeBST(int dimension, const void* key, const void* value){
	kdNodeBST *node = (kdNodeBST*) malloc(sizeof(kdNodeBST));
	if(!node)
		return NULL;
	affecteDimensionkdNodeBST(node, dimension);
	affecteKeykdNodeBST(node, key);
	affecteValuekdNodeBST(node, value);
	affecteFGkdNodeBST(node, NULL);
	affecteFDkdNodeBST(node, NULL);
	return node;
}

void libererkdNodeBST(void* node, bool freeContent){
	if(node!=NULL)
	{
		if(freeContent)
		{
			free((void*) valuekdNodeBST(node));
		}
		free(node);
	}
}

int dimensionkdNodeBST(const kdNodeBST* node){
	return node->dimension; 
}

const void*  keykdNodeBST(const kdNodeBST* node){
	return node->key;
}

const void*  valuekdNodeBST(const kdNodeBST* node){
	return node->value;
}

kdNodeBST* FGkdNodeBST(const kdNodeBST* node){
	return node->fg;
}

kdNodeBST* FDkdNodeBST(const kdNodeBST* node){
	return node->fd;
}

void affecteDimensionkdNodeBST(kdNodeBST* node, int dimension){
	node->dimension = dimension;
}

void affecteKeykdNodeBST(kdNodeBST* node, const void* key){
	node->key = key;
}

void affecteValuekdNodeBST(kdNodeBST* node, const void* value){
	node->value = value;
}

void affecteFGkdNodeBST(kdNodeBST* node, kdNodeBST* fg){
	node->fg = fg;
}

void affecteFDkdNodeBST(kdNodeBST* node, kdNodeBST* fd){
	node->fd = fd;
}